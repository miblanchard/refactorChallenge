# Refactor Notes

Mostly I focused on cleaning up and organizing the code while bringing up to ES6+ best practices.

## Flow

I added flow types to the project so have more certainty about what your components expect as props, and will get any warnings if you try to pass additional props or setState with unspecified values. I moved all of the main components into their own files to help separate concerns and increase re-usability.

## setState(() => {});

Many of my changes related to using the function form of setState and the prevState value passed to the first argument. This ensures that as setState is handled asynchronously you are getting accurate values, vs. using this.state and then directly setting state with a modified version of this. Along a similar line of thought I created new arrays or object inside setState to maintain immutability.

## Stateless Functional Component

I decided to make the TodoList component a SFC because it is simply going to be a product of the props it receives, and I don't expect those props to change expect when the UI for TodoList will also need to update. As a result, I would not really benefit from a shouldComponentUpdate check (whether custom or default single level of PureComponent).
