/* @flow */
// I like to use flow, so I import * as React from "react" to get the types in the import
// Because of that I use React.Componet/React.PureComponent because you can't import * and destructure at the same time
import * as React from "react";
import moment from "moment";
import "./App.css";
import {Timer} from "./components/Timer.js";
import {TodoForm} from "./components/TodoForm.js";
import {TodoList} from "./components/TodoList.js";

export type TodoItem = {|
  index: number,
  value: string,
  date: string,
  done: boolean
|};

type TodoAppState = {|
  showTimer: boolean,
  todoItems: Array<TodoItem>
|};

export class TodoApp extends React.Component<null, TodoAppState> {
  state = {
    showTimer: false,
    todoItems: []
  };

  addTodoItem = (todoItem: *) =>
    this.setState((prevState: TodoAppState) => ({
      todoItems: [
        ...prevState.todoItems,
        {
          index: prevState.todoItems.length,
          value: todoItem,
          date: moment().format("ll"),
          done: false
        }
      ]
    }));

  markTodoDone = (itemIndex: number) =>
    this.setState((prevState: TodoAppState) => {
      const {todoItems} = prevState;
      const todo = todoItems.slice(itemIndex, itemIndex + 1)[0];
      todo.done = !todo.done;
      return {
        todoItems: [
          ...todoItems.slice(0, itemIndex),
          todo,
          ...todoItems.slice(itemIndex + 1)
        ]
      };
    });

  removeTodoItem = (itemIndex: number) =>
    this.setState((prevState: TodoAppState) => {
      const {todoItems} = prevState;
      return {
        todoItems: [
          ...todoItems.slice(0, itemIndex),
          ...todoItems.slice(itemIndex + 1)
        ]
      };
    });

  toggleTimer = () =>
    this.setState((prevState: TodoAppState) => ({
      showTimer: !prevState.showTimer
    }));

  render() {
    const {showTimer, todoItems} = this.state;

    return (
      <div id="main">
        <h1>Todo list</h1>
        <button onClick={this.toggleTimer}>Toggle Timer</button>
        {showTimer ? <Timer /> : null}
        <TodoList
          items={todoItems}
          removeTodoItem={this.removeTodoItem}
          markTodoDone={this.markTodoDone}
        />
        <TodoForm addTodoItem={this.addTodoItem} />
      </div>
    );
  }
}
