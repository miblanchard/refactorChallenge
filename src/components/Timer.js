/* @flow */
import * as React from "react";

type TimerState = {|
  secondCount: number
|};

export class Timer extends React.Component<*, TimerState> {
  state = {
    secondCount: 0
  };

  componentDidMount() {
    this.interval = setInterval(this.updateTimer, 1000);
  }

  componentWillUnmount() {
    if (this.interval) clearInterval(this.interval);
  }

  updateTimer = () => {
    this.setState(prevState => ({
      secondCount: prevState.secondCount + 1
    }));
  };

  interval: *;

  render() {
    return (
      <div>
        <h2>Seconds so Far: {this.state.secondCount}</h2>
      </div>
    );
  }
}
