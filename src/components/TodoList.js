/* @flow */
import * as React from "react";
import {TodoListItem} from "./TodoListItem.js";
import type {TodoItem} from "../App";

export type TodoListProps = {
  items: Array<TodoItem>,
  markTodoDone: (index: number) => void,
  removeTodoItem: (index: number) => void
};

export const TodoList = ({
  items,
  markTodoDone,
  removeTodoItem
}: TodoListProps) => (
  <ul className="list-group">
    {items.map((item, index) => (
      <TodoListItem
        date={item.date}
        done={item.done}
        key={`${item.value} ${index}`}
        index={index}
        markTodoDone={markTodoDone}
        removeTodoItem={removeTodoItem}
        value={item.value}
      />
    ))}
  </ul>
);
