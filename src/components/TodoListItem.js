/* @flow */
import * as React from "react";

type TodoListItemProps = {|
  date: string,
  done: boolean,
  index: number,
  markTodoDone: (index: number) => void,
  removeTodoItem: (index: number) => void,
  value: string
|};

export class TodoListItem extends React.Component<TodoListItemProps> {
  onClickClose = () => {
    const {index, removeTodoItem} = this.props;
    removeTodoItem(index);
  };

  onClickDone = () => {
    const {index, markTodoDone} = this.props;
    markTodoDone(index);
  };

  get todoClass() {
    return this.props.done ? "todoItem done" : "todoItem undone";
  }

  render() {
    const {date, value} = this.props;
    return (
      <li className="list-group-item ">
        <div className={this.todoClass}>
          <span
            className="glyphicon glyphicon-ok icon"
            aria-hidden="true"
            onClick={this.onClickDone}
          />
          <span>{value}</span>
          <span className="date">{`Added: ${date}`}</span>
          <button type="button" className="close" onClick={this.onClickClose}>
            &times;
          </button>
        </div>
      </li>
    );
  }
}
