/* @flow */
import * as React from "react";

type TodoFormProps = {|
  addTodoItem: (text: string) => void
|};

type TodoFormState = {|
  inputText: string
|};

export class TodoForm extends React.Component<TodoFormProps, TodoFormState> {
  state = {
    inputText: ""
  };

  onSubmit = (event: SyntheticEvent<HTMLButtonElement>) => {
    event.preventDefault();
    const {inputText} = this.state;
    if (inputText) {
      this.props.addTodoItem(inputText);
      this.setState({inputText: ""});
    }
  };

  saveInput = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const inputText = event.target.value;
    this.setState({inputText});
  };

  render() {
    return (
      <form
        ref="form"
        id="todoForm"
        onSubmit={this.onSubmit}
        className="form-inline"
      >
        <input
          type="text"
          id="itemName"
          className="form-control"
          placeholder="add a new todo..."
          onChange={this.saveInput}
          value={this.state.inputText}
        />
        <button
          type="submit"
          className="btn btn-default"
          onChange={this.onSubmit}
        >
          Add
        </button>
      </form>
    );
  }
}
